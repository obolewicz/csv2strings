//
//  LanguageDictionaryParser.swift
//  csv2string
//
//  Created by Marcin Obolewicz on 31.01.2018.
//  Copyright © 2018 pl.kfc.KFC. All rights reserved.
//

import Foundation

class LanguageDictionaryParser {
    var languageDictionaries = [LanguageDictionary]()
    var keyReplacement: [String: String]?
    
    init(csv: String, keyReplacement: [String: String]? = nil) {
        self.keyReplacement = keyReplacement
        let rows = csv.components(separatedBy: NewLineSign)
        for row in rows {
            if row == rows.first {
                self.initDictionaries(row)
                continue
            }
            self.addValuesToDictionaries(row)
        }
    }
    
    private func addValuesToDictionaries(_ row: String) {
        let elements = row.components(separatedBy: ColumnSeparator)
        if let key = elements.first {
            let translations = elements.dropFirst()
            for (index, translation) in translations.enumerated() {
                if let languageDictionary = self.languageDictionaries[safe: index] {
                    if let keyReplacement = self.keyReplacement {
                        if let newKey = keyReplacement[key] {
                            languageDictionary.dictionary[newKey] = translation
                        }
                    }
                    else {
                        if key != "" {
                            languageDictionary.dictionary[key] = translation
                        }
                    }
                }
            }
        }
    }
    
    private func initDictionaries(_ row: String) {
        let elements = row.components(separatedBy: ";")
        let languages = elements.dropFirst()
        languages
            .filter { $0 != "" }
            .forEach {
                self.languageDictionaries.append(LanguageDictionary(name: $0))
            }
    }
    
    public func saveToFile() {
        self.languageDictionaries.forEach {
            $0.generateLocalizable()
        }
    }
}
