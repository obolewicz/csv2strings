//
//  LanguageDictionary.swift
//  csv2string
//
//  Created by Marcin Obolewicz on 31.01.2018.
//  Copyright © 2018 pl.kfc.KFC. All rights reserved.
//

import Foundation

class LanguageDictionary {
    var checkPrefix = true
    
    let name: String
    var dictionary = [String: String]()
    
    init(name: String, checkPrefix: Bool = true) {
        self.checkPrefix = checkPrefix
        self.name = name
    }
    
    func generateLocalizable() {
        var result = ""
        var keyPrefix = ""
        self.dictionary
            .sorted(by: <)
            .forEach { (key, value) in
                if self.checkPrefix {
                    if let newKeyPrefix = key.components(separatedBy: ".").first {
                        if newKeyPrefix != keyPrefix {
                            keyPrefix = newKeyPrefix
                            let mark = newKeyPrefix.replacingOccurrences(of: "_", with: " ")
                            result = result + "\n// MARK: - \(mark)\n"
                        }
                    }
                }
                
            result = result + "\"\(key)\" = \"\(value)\";\n"
        }
        self.saveToFile(result)
    }
    
    func saveToFile(_ string: String) {
        let filename = self.name + ".strings"
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(filename)
            do {
                try string.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
                print(filename + " saved in default documets directory")
            } catch {
                print("Unable to save output file")
            }
        }
    }
}
