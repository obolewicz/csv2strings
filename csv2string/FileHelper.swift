//
//  FileHelper.swift
//  csv2string
//
//  Created by Marcin Obolewicz on 31.01.2018.
//  Copyright © 2018 pl.kfc.KFC. All rights reserved.
//

import Foundation

class FileHelper {
    static let csvExtension = "csv"
    static func openCSVFile(name: String?) -> String? {
        if let fileArg = name,
            let path:String = Bundle.main.path(forResource: fileArg, ofType: csvExtension) {
            do {
                let string = try String(contentsOfFile: path, encoding: .utf8)
                return string
            } catch {
                print("Unable to open \(fileArg).\(csvExtension)")
            }
        }
        else {
            if let name = name {
                print("Unable to find \(name).\(csvExtension)")
            }
        }
        return nil
    }
}
