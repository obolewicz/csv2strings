//
//  main.swift
//  csv2string
//
//  Created by Marcin Obolewicz on 31.01.2018.
//  Copyright © 2018 pl.kfc.KFC. All rights reserved.
//

import Foundation

let NewLineSign = "\r\n"
let ColumnSeparator = ";"

if let translations = FileHelper.openCSVFile(name: CommandLine.arguments[safe: 1]) {
    var keysDictionary: [String:String]? = nil
    if let keys = FileHelper.openCSVFile(name: CommandLine.arguments[safe: 2]) {
        let ignoredNewKeys = ["ANDROID"]
        keysDictionary = keys.createKeysDictionary(ignoredNewKeys: ignoredNewKeys)
    }
    let parser = LanguageDictionaryParser(csv: translations, keyReplacement: keysDictionary)

    parser.saveToFile()
}
else {
    print("Please provide translation file name (without .csv extension) as first argument and optionally second one with key's replacements filename")
}

