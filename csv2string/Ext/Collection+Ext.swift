//
//  Collection+Ext.swift
//  csv2string
//
//  Created by Marcin Obolewicz on 31.01.2018.
//  Copyright © 2018 pl.kfc.KFC. All rights reserved.
//

import Foundation

extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
