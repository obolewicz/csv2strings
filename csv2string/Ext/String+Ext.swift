//
//  String+Ext.swift
//  csv2string
//
//  Created by Marcin Obolewicz on 31.01.2018.
//  Copyright © 2018 pl.kfc.KFC. All rights reserved.
//

import Foundation

extension String {
    
    func createKeysDictionary(ignoredNewKeys: [String] = []) -> [String:String] {
        var result: [String:String] = [:]
        let rows = self.components(separatedBy: NewLineSign)
        rows.forEach({ row in
            let columns = row.components(separatedBy: ColumnSeparator)
            if let oldKey = columns[safe:1],
                let newKey = columns.first {
                if newKey != "" && oldKey != "" {
                    if !ignoredNewKeys.contains(newKey) {
                        result[oldKey] = newKey
                    }
                }
            }
        })
        return result
    }
}

